#!/bin/bash

FOO=hello/world/this/is/a/script
# replace all forward slashes with dashes
BAR=${FOO//\//-}
echo "${BAR}"
if [[ "${BAR}" != "hello-world-this-is-a-script" ]]; then
    exit 1
fi
